import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_EMotor.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

sensorid = "1ee847be-fddd-6ee4-892a-68c4555b0981"

sensdata = {
    sensorid: {
        "acceleration_x": [],
        "acceleration_y": [],
        "acceleration_z": [],
        "timestamp": [],
    }
}
# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

starttime = time.time()
passedtime = 0

while passedtime < measure_duration_in_s:
    sensdata[sensorid]["acceleration_x"].append(accelerometer.acceleration[0])
    sensdata[sensorid]["acceleration_y"].append(accelerometer.acceleration[1])
    sensdata[sensorid]["acceleration_z"].append(accelerometer.acceleration[2])
    sensdata[sensorid]["timestamp"].append(passedtime)
    
    time.sleep(0.001)
    passedtime = time.time()-starttime
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

with h5py.File(path_h5_file, 'w') as f:
    
    group = f.create_group("RawData")
    x = group.create_dataset("acceleration_x", data = sensdata[sensorid]["acceleration_x"])
    y = group.create_dataset("acceleration_y", data = sensdata[sensorid]["acceleration_y"])
    z = group.create_dataset("acceleration_z", data = sensdata[sensorid]["acceleration_z"])
    time = group.create_dataset("timestamp", data = sensdata[sensorid]["timestamp"])
    x.attrs['units'] = "m/s²"
    y.attrs['units'] = "m/s²"
    z.attrs['units'] = "m/s²"
    time.attrs['units'] = "s"
# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
